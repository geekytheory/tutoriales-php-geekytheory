<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<?php

	$variable;
	
	//una variable en php siempre contendra como caracter inical $. En php no hace falta declar el tipo de variable, el tipo de esta se gestiona de manera automática de tal forma que:
	
	$variable = 3; //variable de tipo int
	
	$variable = 3.7;// variable de tipo float
	
	$variable = "GeeKytheory"; // variable de tipo String;
	
	//Ejemplo de tabla de multiplicar dinámica.
	
	$indice = 10;
	$multiplicando = 20;
	
	//creamos una tabla en html
	
	echo "<table border = 2>";
	
	
	//Le ponemos los titulos a las cabeceras de las columnas
	
	echo "<th> Tabla * </th>";
	echo "<th> X1 </th>";
	echo "<th> X2 </th>";
	echo "<th> X3 </th>";
	echo "<th> X4 </th>";
	echo "<th> X5 </th>";
	echo "<th> X6 </th>";
	echo "<th> X7 </th>";
	echo "<th> X8 </th>";
	echo "<th> X9 </th>";
	echo "<th> X10 </th>";
				
	
	//mediante dos bucles "for" crearemos las filas de la columnas de forma dinámica
	
		for($i=1;$i<=$multiplicando;$i++){
			
					
			//creamos una fila
			echo "<tr>";	
			
			//creamos el segundo bucle for que vaya calculando el contenido de cada columna
			
			//nombre de la fila
			
			echo "<td>";
			echo "<b>Tabla del ".$i."</b>";
			echo "</td>";
			
			for($j=1;$j<=$indice;$j++){
				
				//creamos una columna
				echo "<td>";
				//multiplicamos el "j" por la iteracion en la que estemos "i" de la tabla de multiplicar
				echo $j*$i;
				echo "</td>";
				
			}
			
			//cerramos la etiqueta tr
			echo "</tr>";
			
		}
		
	//cerramos la etiqueta table
	
	echo "</table>";
		
?>

<body>
</body>
</html>