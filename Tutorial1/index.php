<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tutorial 1 - PHP: Introducción</title>
</head>

<body>

	<?php //con la etiqueta <?php indicamos al server que interprete el codigo aquí expuesto cómo código php
	
	//la sentencia echo imprime en pantalla el contenido del mismo, siendo posible incluir código html en el.
	
	echo '<b> HOLA AMIGOS DE GEEKYTHEORY </b>';
	
	
	/*con la etiqueta ?> cerraremos indicaremos al servdor que deje de interpretar el código php */ 
	?> 

</body>
</html>
